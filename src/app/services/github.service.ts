import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';

import { IRepositoryItem, IIssueItem, ICommit } from './../interfaces/github.interfaces';

@Injectable()
export class GitHubService {
  private searchUrl = 'https://api.github.com/search/repositories?q=';
  private issuesUrl = 'https://api.github.com/search/issues?q=repo:';

  constructor(private http: Http) { }

  search(repository_name: string) {
    return this.http.get(this.searchUrl + repository_name)
            .map((response: Response) => {
              return response.json().items as IRepositoryItem[];
            })
            .catch(e => Observable.throw(e));
  }

  getIssues(full_name: string) {
    const url = this.issuesUrl + full_name;
    return this.http.get(url)
            .map((response: Response) => {
              return response.json().items as IIssueItem[];
            })
            .catch(e => Observable.throw(e));
  }

  getCommits(full_name: string) {
    const url = `https://api.github.com/repos/${full_name}/commits`;
    return this.http.get(url)
            .map((res: Response) => {
              return res.json() as ICommit[];
            })
            .catch(e => Observable.throw(e));
  }

}
