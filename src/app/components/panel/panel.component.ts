import { Component, trigger, state, style, transition, animate} from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css'],
  animations: [
    trigger('expandCollapse', [
      state('collapsed, void', style({height: '0px'})),
      state('expanded', style({height: '*'})),
      transition('collapsed <=> expanded', animate('250ms ease'))
    ])
  ]
})
export class PanelComponent {
  stateExpression: string = 'collapsed';

  toggleCollapse() {
    this.stateExpression = this.stateExpression === 'collapsed' ? 'expanded' : 'collapsed';
  }

}
