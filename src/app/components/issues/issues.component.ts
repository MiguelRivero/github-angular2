import { Component, Input, OnChanges, SimpleChange } from '@angular/core';
import { Store } from '@ngrx/store';
import { GitHubService } from './../../services/github.service';
import { IRepositoryItem, IIssueItem, ILabel } from './../../interfaces/github.interfaces';
import { AppState } from './../../app.state';
import { IssuesActions } from './../../actions/issues.actions';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnChanges {
  @Input() repository: IRepositoryItem = null;
  @Input() fetchedIssues: {[key: string]: IIssueItem[]} = {};
  issues: IIssueItem[];
  isLoading: boolean = true;
  isEmpty: boolean;

  constructor(private store: Store<AppState>,
              private issueService: GitHubService) {}

  hasLabels(issue: IIssueItem) {
    return issue.labels.length > 0;
  }

  getColor(label: ILabel) {
    return '#' + label.color;
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}) {
    if (changes['repository'] && changes['repository'].currentValue !== null) {
      if (this.fetchedIssues[this.repository.full_name]) {
          this.isLoading = false;
          this.issues = this.fetchedIssues[this.repository.full_name];
          this.isEmpty = this.issues.length > 0 ? false : true;
      } else {
        this.isLoading = true;
        this.issueService.getIssues(this.repository.full_name)
          .subscribe(
            (data) => {
              this.store.dispatch(
                IssuesActions.addIssues({
                  [this.repository.full_name]: data
                })
              );
              this.isLoading = false;
              this.issues = data;
              this.isEmpty = data.length > 0 ? false : true;
            }
          );
      }
    }
  }
}
