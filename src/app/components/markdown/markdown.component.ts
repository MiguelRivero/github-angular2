import { Component, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import * as marked from 'marked';


@Component({
  selector: 'app-markdown',
  template: '<ng-content></ng-content>',
  styleUrls: ['./markdown.component.css']
})
export class MarkdownComponent implements OnInit, AfterViewInit {

  private md: any;
  constructor(private el: ElementRef) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.md = this.prepare(this.el.nativeElement.innerHTML);
    this.el.nativeElement.innerHTML = marked(this.md);
  }

  prepare(raw: string) {
    return raw.split('\n').map((line) => line.trim()).join('\n');
  }
}
