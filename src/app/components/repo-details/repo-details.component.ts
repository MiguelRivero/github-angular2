import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Store } from '@ngrx/store';

import { AppState } from './../../app.state';
import { IRepositoryItem, IIssueItem, ICommit } from './../../interfaces/github.interfaces';
import { getIssues } from './../../selectors/issues.selector';
import { getCommits } from './../../selectors/commits.selector';

@Component({
  selector: 'app-repo-details',
  templateUrl: './repo-details.component.html',
  styleUrls: ['./repo-details.component.css']
})
export class RepoDetailsComponent implements OnInit {
  @Input() repository: IRepositoryItem = null;
  selectedTab: string = 'global';
  storedIssues$: Observable<IIssueItem[]>;
  storedCommits$: Observable<ICommit[]>;

  constructor(private store: Store<AppState>) {
   }

  ngOnInit() {
    this.storedIssues$ = this.store.let(getIssues);
    this.storedCommits$ = this.store.let(getCommits);
  }

  selectTab(tab: string) {
    this.selectedTab = tab;
  }

  isSelectedTab(tab) {
    return this.selectedTab === tab;
  }
}
