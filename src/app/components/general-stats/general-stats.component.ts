import { Component, OnInit, Input } from '@angular/core';
import { IRepositoryItem } from './../../interfaces/github.interfaces';

@Component({
  selector: 'app-general-stats',
  templateUrl: './general-stats.component.html',
  styleUrls: ['./general-stats.component.css']
})
export class GeneralStatsComponent implements OnInit {
  @Input() repository: IRepositoryItem;
  constructor() { }

  ngOnInit() {
  }

}
