import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';
import { GitHubService } from './../../services/github.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { IRepositoryItem } from './../../interfaces';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  repositories$: Observable<any>;
  searchResult: IRepositoryItem[] = [];
  private searchTerms = new Subject<string>();
  @Input() selectedRepository: IRepositoryItem;

  @Output() selectedRepositoryChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private searchService: GitHubService) { }

  ngOnInit() {
    this.repositories$ = this.searchTerms
      .debounceTime(500)        // wait for 300ms pause in events
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(name => {
        return name   // switch to new observable each time
        // return the http search observable
        ? this.searchService.search(name)
        // or the observable of empty heroes if no search term
        : Observable.of<any[]>([]);
      })
      .catch(error => {
        // TODO: real error handling
        return Observable.of<any[]>([]);
      });
  }

   // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  selectRepository(repository: IRepositoryItem) {
    this.selectedRepository = repository;
    this.selectedRepositoryChange.emit(repository);
  }
}
