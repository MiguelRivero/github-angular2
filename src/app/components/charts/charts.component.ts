import { CommitsActions } from './../../actions/commits.actions';
import { Component, Input, OnChanges, SimpleChange } from '@angular/core';
import { Store } from '@ngrx/store';
import { GitHubService } from './../../services/github.service';
import { AppState } from './../../app.state';
import { IRepositoryItem,  ICommit } from './../../interfaces/github.interfaces';
import * as _ from 'lodash';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnChanges {
  @Input() repository: IRepositoryItem = null;
  @Input() fetchedCommits = {};
  commits: ICommit[] = [];
  isLoading: boolean = true;

  get isEmpty(): boolean{
    return this.commits.length > 0 ? false : true ;
  }

  // Chart config and data
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = []; // ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartData: any[] = [
    {data: [], label: ''},
    // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Label 1'}
  ];


  constructor(private store: Store<AppState>,
              private githubService: GitHubService) { }

  ngOnChanges(changes: {[propName: string]: SimpleChange}) {
    if (changes['repository'] && changes['repository'].currentValue !== null) {
      if (this.fetchedCommits[this.repository.full_name]) {
          this.isLoading = false;
          this.commits = this.fetchedCommits[this.repository.full_name];
          this.updateChart();
      } else {
        this.isLoading = true;
        this.githubService.getCommits(this.repository.full_name)
          .subscribe(
            (data) => {
              // Store the fetched commits in the Store
              this.store.dispatch(
                CommitsActions.addCommits({
                  [this.repository.full_name]: data
                })
              );
              this.isLoading = false;
              this.commits = data;
              this.updateChart();
            }
          );
      }
    }
  }

  generateData() {
    let data = {};
    let dataArray = [];

    for (let commit of this.commits) {
      let date = commit.commit.committer.date.slice(0, 10);
      let previous_value = data[date] === undefined ? 0 : data[date] ;
      data[date] = previous_value + 1;
    }
    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        dataArray.push({
          x: key,
          y: data[key]
        });
      }
    }
    return dataArray;
  }

  updateChart() {
    let x = <string[]> _.map(this.generateData(), 'x');
    let y = _.map(this.generateData(), 'y');

    this.barChartLabels = x;
    this.barChartData = [{data: y, label: 'Latest 30 commits'}];
  }
}
