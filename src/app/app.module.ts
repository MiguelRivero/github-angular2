// import { Ng2-ChartsComponent } from 'ng2-ChartsComponent';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { StoreModule } from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { StoreLogMonitorModule, useLogMonitor } from '@ngrx/store-log-monitor';
import { Ng2PaginationModule } from 'ng2-pagination';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { AppComponent } from './app.component';
import { SearchComponent } from './components/search/search.component';
import { RepoDetailsComponent } from './components/repo-details/repo-details.component';
import { GeneralStatsComponent } from './components/general-stats/general-stats.component';
import { IssuesComponent } from './components/issues/issues.component';
import { ChartsComponent } from './components/charts/charts.component';
import { GitHubService } from './services/github.service';
import { issuesReducer } from './reducers/issues.reducer';
import { commitsReducer } from './reducers/commits.reducer';
import { MarkdownComponent } from './components/markdown/markdown.component';
import { PanelComponent } from './components/panel/panel.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    RepoDetailsComponent,
    GeneralStatsComponent,
    IssuesComponent,
    ChartsComponent,
    MarkdownComponent,
    PanelComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2PaginationModule,
    ChartsModule,
    StoreModule.provideStore({
      issues: issuesReducer,
      commits: commitsReducer
    }),
    StoreDevtoolsModule.instrumentStore({
      monitor: useLogMonitor({
        visible: false,
        position: 'left'
      })
    }),
    StoreLogMonitorModule
  ],
  providers: [GitHubService],
  bootstrap: [AppComponent]
})
export class AppModule { }
