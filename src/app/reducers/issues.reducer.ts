import { ActionReducer, Action } from '@ngrx/store';
import { IssuesActions } from './../actions/issues.actions';
import { IIssueItem } from './../interfaces/github.interfaces';

export const initialState: {[key: string]: IIssueItem[]} = {};

export const issuesReducer: ActionReducer<{[key: string]: IIssueItem[]}> =
             (state: {[key: string]: IIssueItem[]} = initialState, action: Action) => {
    switch (action.type) {
        case IssuesActions.ADD_ISSUES:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
};
