import { ActionReducer, Action } from '@ngrx/store';
import { CommitsActions } from './../actions/commits.actions';
import { ICommit } from './../interfaces/github.interfaces';

export const initialState: {[key: string]: ICommit[]} = {};

export const commitsReducer: ActionReducer<{[key: string]: ICommit[]}> =
             (state: {[key: string]: ICommit[]} = initialState, action: Action) => {
    switch (action.type) {
        case CommitsActions.ADD_COMMITS:
            return Object.assign({}, state, action.payload);
        default:
            return state;
    }
};
