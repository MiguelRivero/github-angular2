import { Component } from '@angular/core';
import { IRepositoryItem } from './interfaces/github.interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  selectedRepository: IRepositoryItem = null;

  handleSelectRepository(repo: IRepositoryItem) {
    this.selectedRepository = repo;
  }
}
