import { IIssueItem, ICommit } from './interfaces/github.interfaces';

export interface AppState {
  issues: IIssueItem[];
  commits: ICommit[];
}
