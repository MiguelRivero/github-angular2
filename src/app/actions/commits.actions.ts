import { Action } from '@ngrx/store';
import { ICommit } from './../interfaces/github.interfaces';

export class CommitsActions {
    static ADD_COMMITS = 'ADD_COMMITS';

    static addCommits = (commits: {[key: string]: ICommit[]}): Action => {
        return {
            type: CommitsActions.ADD_COMMITS,
            payload: commits
        };
    }
}
