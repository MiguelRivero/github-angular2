import { Action } from '@ngrx/store';
import { IIssueItem } from './../interfaces/github.interfaces';

export class IssuesActions {
    static ADD_ISSUES = 'ADD_ISSUES';

    static addIssues = (issues: {[key: string]: IIssueItem[]}): Action => {
        return {
            type: IssuesActions.ADD_ISSUES,
            payload: issues
        };
    }
}
