import { describe, expect, it } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { getIssues } from './issues.selector';


describe('Selector: Issues', () => {

    it('::getIssues | It should retrieve all the issues', () => {
        const issues = {
            'angular/angular': {
                archive_url: 'foo',
                assignees_url: 'sed',
                blobs_url: 'bla'
            }
        };
        const state$ = Observable.of({
            issues: issues
        });


        state$.let(getIssues)
            .subscribe(
                (state) => {
                    expect(state).toEqual(issues);
                }
            );

    });

});
