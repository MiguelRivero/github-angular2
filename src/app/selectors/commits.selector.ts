import { Observable } from 'rxjs/Observable';
import { AppState } from './../app.state';
import { ICommit } from './../interfaces/github.interfaces';

export const getCommits = (state: Observable<AppState>): Observable<ICommit[]> => {
    return state.map((s: AppState) => s.commits);
};
