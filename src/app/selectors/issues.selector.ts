import { Observable } from 'rxjs/Observable';
import { AppState } from './../app.state';
import { IIssueItem } from './../interfaces/github.interfaces';

export const getIssues = (state: Observable<AppState>): Observable<IIssueItem[]> => {
    return state.map((s: AppState) => s.issues);
};
